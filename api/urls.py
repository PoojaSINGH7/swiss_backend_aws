"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.contrib import admin
from core import views as core_views
from tournament import views as tournament_views

urlpatterns = [
    url(r'^api/signup$', core_views.sign_up_view),
    url(r'^api/login$', core_views.login_view),
    url(r'^api/logout$', core_views.logout_view),
    url(r'^api/tournaments/$',
        tournament_views.tournaments_view),
    url(r'^api/tournaments/(?P<tournament_id>[0-9]+)/$',
        tournament_views.status_view),
    #url(r'^api/tournaments/id?details=true$', tournament_views.)
    url(r'^api/players/(?P<tournament_id>[0-9]+)$', tournament_views.player_view),
    url(r'^api/matches/$', tournament_views.match_view),
    url(r'^api/tournaments/(?P<tour_id>[0-9]+)/$',
        tournament_views.tour_status),
    # url(r'^api/matches/(?P<tournament_id>[0-9]+)/(?P<round_no>[0-9]+)/$',
    #     tournament_views.getmatch_view),
    url(r'^api/standings/$',
        tournament_views.standings_view),
    url(r'^api/swiss_pairings$',
        tournament_views.swiss_pairings_view),
    url(r'^api/matches/(?P<tournament_id>[0-9]+)/(?P<round_no>[0-9]+)$',
        tournament_views.match_view),
    url(r'^api/players/$',
        tournament_views.getplayers_view),
    # url(r'^api/match/(?P<tournament_id>[0-9]+)/(?P<round_no>[0-9]+)/$',
    #     tournament_views.report_match_view),
    url(r'^api/reset_db/$', tournament_views.reset_db),
    url(r'^api/hello/$', core_views.hello_view),
    url(r'^api/currentround/(?P<tournament_id>[0-9]+)$',
        tournament_views.currentround_view),
    url(r'^api/match/(?P<tournament_id>[0-9]+)/(?P<round_num>[0-9]+)$', 
        tournament_views.roundresult_view),
    url(r'^api/tour_status/(?P<tournament_id>[0-9]+)$',tournament_views.tour_status),
    # url(r'^api/standings/(?P<tournament_id>[0-9]+)/(?P<round_no>[0-9]+)/$',
    #     tournament_views.tour_standings_view),
    #url(r'^api/matches/(?P<tournament_id>[0-9]+)/(?P<round_no>[0-9]+)/$', tournament_views.match_view)
    #url(r'^api/swiss_pairings(?P<round_ no>[0-9]+)&(?P<tournament_id>[0-9]+)/$', tournament_views.swiss_pairings_view)
    #url(r'^admin/', admin.site.urls)

]
