import unittest
import requests
import json


BASE_URL = 'http://localhost:8888'


class BaseSwissTest(unittest.TestCase):

    def setUp(self):
        auth = {
            'username': 'u1',
            'password': 'hello123',
            'fullname': 'u1u1',
            'country': 'nz',
        }

        r = requests.post(
            "{}/api/signup/".format(BASE_URL),
            json=auth
        )
        # auth = {
        #     'username': 'u1',
        #     'password': 'hello123'
        # }
        # r = requests.post(
        #     "{}/api/login/".format(BASE_URL),
        #     json=auth
        # )
        data = self.resp_to_json(r)
        self.token = data['token']

    def resp_to_json(self, r):
        return json.loads(r.content.decode('utf-8'))

    def tearDown(self):
        requests.delete(
            "{}/api/reset_db/".format(BASE_URL)
        )


class LoginTest(BaseSwissTest):
    def test_hello_view(self):
        payload = {'username': 'u1', 'password': 'hello123'}
        r = requests.get("{}/api/hello/".format(BASE_URL), headers={
            'X-Auth-Token': self.token
        }, json=payload)
        data = self.resp_to_json(r)
        print(data)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(data['hello'], 'world')
        r = requests.get("{}/api/hello/".format(BASE_URL), headers={
        }, json=payload)
        self.assertEqual(r.status_code, 400)
        self.assertEqual(self.resp_to_json(r)['error'], "token not supplied")
# class LoginTest(BaseSwissTest):

#    def test_hello_view(self):
#        payload = {'username': 'u1', 'password': 'hello123'}
#        r = requests.get("{}/api/login/".format(BASE_URL), headers={
#            'X-Auth-Token': self.token
#        }, json=payload)
#        data = self.resp_to_json(r)
#        self.token = data['token']
#        r = requests.get("{}/api/hello/".format(BASE_URL), headers={
#            'X-Auth-Token': self.token
#        })
#        data = self.resp_to_json(r)
#        print(data)
#        # self.assertEqual(r.status_code, 200)
#        # self.assertEqual(data['hello'], 'world')


class CreateTournamentTest(BaseSwissTest):

    def test_create_tournament(self):
        payload = {'tournament_name': 'ipl'}
        r = requests.post("{}/api/home/".format(BASE_URL), headers={
            'X-Auth-Token': self.token
        }, json=payload)
        self.assertEqual(r.status_code, 200)
        content = self.resp_to_json(r)
        self.assertEqual(content['name'], payload['tournament_name'])


if __name__ == '__main__':
    unittest.main()
