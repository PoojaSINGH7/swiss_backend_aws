from django.db import models
from django.contrib.auth.models import User
from core.decorators import token_required
from django.db.models import Q
import pdb

TOURNAMENT_STATUS_CHOICES = (
    (1, 'NOT STARTED'),
    (2, 'STARTED'),
    (3, 'COMPLETED')
)


class Tournament(models.Model):
    name = models.CharField(max_length=100)
    creator = models.ForeignKey(User)
    status = models.IntegerField(choices=TOURNAMENT_STATUS_CHOICES, default=1)
    current_round = models.IntegerField(default=1)

    def __str__(self):
        return '{} - {} - Status: {}'.format(
            self.name,
            self.creator,
            self.status
        )

    def player_standings(self, round_no):
        players = []
        player_wins_dict = {}
        #import pdb;pdb.set_trace()
        #prev_round = int(round_no) - 1
        for player in self.players.all():
            win_count = self.games.filter(
                            winner=player.player,
                            round__lt=round_no).count()
            #prev_round = int(round_no) - 1
            total_count = self.games.filter(
                            Q(player_one=player.player) |
                            Q(player_two=player.player),
                            round__lt=round_no).count()
            player_wins_dict[player] = {
                                        "w": win_count,
                                        "t": total_count,
                                        "l": total_count - win_count
                                        }
        #import pdb;pdb.set_trace()
        players = (sorted(player_wins_dict.items(), key=lambda t: t[1]["w"], reverse=True))
        print(players)
        return players

    def swiss_pairings(self, round_no):
        pairings_list = []
        pairs = []
        #import pdb;pdb.set_trace()
        player_list = self.player_standings(round_no)
        length = len(player_list)
        print(player_list)
        for i in range(0, int(length / 2)):
            pairs = []
            #import pdb;pdb.set_trace()
            player1 = player_list[0][0]
            player1 = player1.player
            player_list.remove(player_list[0])
            for player in player_list:
                player2 = player[0]
                player2 = player2.player
                valid_pair = self.has_played_before(player1,
                                                    player2,
                                                    round_no)
                if valid_pair is True:
                    pairs.append(player1.name)
                    pairs.append(player2.name)
                    pairings_list.append(pairs)
                    player_list.remove(player)
                    break
                else:
                    #player_list.remove(player)
                    continue
        return pairings_list

    def report_match(self, player1, player2, winner, round_num):
        match_obj = Match.objects.create(tournament=self,
                             player_one=player1,
                             player_two=player2,
                             winner=winner,
                             round=round_num)
        match_obj.status = 2
        match_obj.save()

    def has_played_before(self, player1, player2, round_no):
        #prev_round = int(round_no) - 1
        self.match_played = self.games.filter(Q(player_one=player1) |
                                              Q(player_one=player2),
                                              Q(player_two=player1) |
                                              Q(player_two=player2), round__lt=round_no)
        if self.match_played:
            return False
        else:
            return True


    class Meta:
        unique_together = ('creator', 'name')


class Player(models.Model):
    name = models.CharField(max_length=50)
    creator = models.ForeignKey(User)

    class Meta:
        unique_together = ('name', 'creator')

    def __str__(self):
        return '{} - {}'.format(self.name, self.creator)


class TournamentPlayer(models.Model):
    tournament = models.ForeignKey(Tournament, related_name='players')
    player = models.ForeignKey(Player)

    class Meta:
        unique_together = ('tournament', 'player')


MATCH_STATUS_CHOICES = (
    (1, 'UPCOMING'),
    (2, 'CONDUCTED')
)


class Match(models.Model):
    tournament = models.ForeignKey(Tournament, related_name='games')
    status = models.IntegerField(choices=MATCH_STATUS_CHOICES, default=1)
    round = models.IntegerField()
    player_one = models.ForeignKey(Player, related_name='player_one')
    player_two = models.ForeignKey(Player, related_name='player_two')
    winner = models.ForeignKey(Player, related_name='winner')

    def __str__(self):
        return '{} {}'.format(
            self.tournament,
            self.round
        )

