from django.shortcuts import render
from django.http import JsonResponse
from core.decorators import token_required
from .models import Tournament, Player, TournamentPlayer, Match
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from core.models import UserProfile
from django.db.models import Q
import json
import math
import pdb


def ispower(n, base):

    if n == base:
        return True

    if base == 1:
        return False

    temp = base

    while (temp <= n):
        if temp == n:
            return True
        temp *= base

    return False


@token_required
def  tournaments_view(request):
    if request.method == 'GET':
        tour_list = []
        tournament_names_list = Tournament.objects.filter(
            creator=request.swiss_user)
        #if tournament_names_list:
        for tour_name in tournament_names_list:
            tour_names = {}
            tour_names['name'] = tour_name.name
            tour_names['id'] = tour_name.id
            tour_names['status'] = tour_name.status
            #tour_names['current_round'] = tour_names.current_round
            tour_list.append(tour_names)
        return JsonResponse(tour_list, safe=False, status=200)
        # else:
        #     return JsonResponse("No tournament exists for this user now, create a tournament", safe=False)
    elif request.method == 'POST':
        req_body = json.loads(request.body.decode('utf-8'))
        tournament_name = req_body['name']
        try:
            tournament = Tournament.objects.get(name=tournament_name,
                                                creator=request.swiss_user)
        except ObjectDoesNotExist:
            tournament = Tournament.objects.create(name=tournament_name,
                                                   creator=request.swiss_user)
            return JsonResponse({'name': tournament.name, 'id': tournament.id, 'status': tournament.status},
                                status=200)
        else:
            return JsonResponse({'error': "This tournament already exists"},
                                status=400)


@token_required
def status_view(request, tournament_id):
    req_body = json.loads(request.body.decode('utf-8'))
    name = req_body['name']
    status = req_body['status']
    tournament_obj = Tournament.objects.get(id=tournament_id)
    if tournament_obj.status != status:
        Tournament.objects.filter(id=tournament_id, name=name).update(status=status)
        return JsonResponse({'status': 'Updated Status'})
    else:
        return JsonResponse("Duplicate tournament name",safe=False)


@token_required
def player_view(request, tournament_id):
    if request.method == 'POST':
        req_body = json.loads(request.body.decode('utf-8'))
        #tournament_id = req_body['tournament_id']
        player_name = req_body['name']
        player_id = req_body.get('player_id')
        tour_obj = Tournament.objects.get(id=tournament_id)
        tour_name = tour_obj.name
        if player_id:
            try:
                player = Player.objects.get(
                            id=player_id,
                            name=player_name,
                            creator=request.swiss_user
                        )
                TournamentPlayer.objects.create(
                                tournament=tour_obj,
                                player=player)
                return JsonResponse(
                    {
                        'playername': player.name,
                        'playerid': player.id
                    }, status=200)
            except ObjectDoesNotExist:
                return JsonResponse(
                    {'error': 'either player id is incorrect'},
                    status=400)
        elif player_name:
            tour_status = tour_obj.status
            try:
                player = Player.objects.get(
                            name=player_name,
                            creator=request.swiss_user
                        )
                TournamentPlayer.objects.get(
                                tournament=tour_obj,
                                player=player)
            except ObjectDoesNotExist:
                if tour_status == 1:
                    player = Player.objects.create(
                                name=player_name,
                                creator=request.swiss_user
                            )
                    TournamentPlayer.objects.create(
                                    tournament=tour_obj,
                                    player=player)
                    return JsonResponse(
                        {
                            'playername': player.name,
                            'playerid': player.id
                        }, status=200)
                else:
                    return JsonResponse("Cannot add players now," 
                        "tournament either in progress or completed",status=400,safe=False)
       
        # else:
        #     return JsonResponse(
        #         {'error': 'either player name or id must be provided'},
        #         status=400)
    else:
        tour_players_list = []
        # tournament_obj = Player.objects.filter(
        #     creator=request.swiss_user)
        tour_obj =Tournament.objects.get(id=tournament_id)
        tournament_obj = TournamentPlayer.objects.filter(tournament=tour_obj)
        #if tournament_obj:
        for tour_player in tournament_obj.all():
            tour_players = {}
            tour_players['name'] = tour_player.player.name
            tour_players['id'] = tour_player.player.id
            tour_players_list.append(tour_players)
        return JsonResponse(tour_players_list, safe=False, status=200)
        # else:
        #     return JsonResponse("No existing Players for this tournament",safe=False)


@token_required
def match_view(request):
        # import pdb; pdb.set_trace()
        req_body = json.loads(request.body.decode('utf-8'))
        player_playing = []
        tournament_id = req_body['tournament_id']
        player_one_id = req_body['player_one_id']
        player_two_id = req_body['player_two_id']
        winner_id = req_body['winner_id']
        #loser_id = req_body['loser_id']
        round_num = req_body['round_num']
        player_playing.append(player_one_id)
        player_playing.append(player_two_id)
        tournament_obj = Tournament.objects.get(
                            id=tournament_id,
                            creator=request.swiss_user
        )
        no_of_players = TournamentPlayer.objects.filter(
                            tournament=tournament_obj
                        ).count()
        #import pdb;pdb.set_trace()
        match_for_each_round = int(no_of_players / 2)
        check_no_of_players = ispower(no_of_players, 2)
        total_rounds = int(math.log(no_of_players, 2))
        pairings = tournament_obj.swiss_pairings(round_num)
        print(pairings)
        print(total_rounds)
        #return JsonResponse({'d':pairings})
        #pair_list = pairings[0]
        #import pdb;pdb.set_trace()
        if check_no_of_players:
            res = 0
            for pair in pairings:
                print(pair)
                if (pair[0] in player_playing and pair[1] in player_playing):
                    res = 1
                    break
            if res == 1:
                player1 = tournament_obj.players.get(player__name=player_one_id)
                player2 = tournament_obj.players.get(player__name=player_two_id)
                winner = tournament_obj.players.get(player__name=winner_id)
                played_before = tournament_obj.has_played_before(player1.player,
                                                                 player2.player,
                                                                 round_num)
                if played_before:
                    tournament_obj.report_match(player1.player, player2.player,
                                                winner.player, round_num)
                    if (tournament_obj.current_round < total_rounds) and tournament_obj != 1:
                        Tournament.objects.filter(id=tournament_obj.id).update(status=2)
                    if (total_rounds == tournament_obj.current_round):
                        Tournament.objects.filter(id=tournament_obj.id).update(status=3)
                    round_check = tournament_obj.games.filter(round=round_num).count()
                    if (round_check == match_for_each_round):
                        next_round = tournament_obj.current_round + 1
                        #import pdb;pdb.set_trace()
                        Tournament.objects.filter(id=tournament_obj.id).update(current_round=next_round)
                    return JsonResponse({'success': 'true'}, status=200)
                else:
                    return JsonResponse("These Players have played earlier in the same round",
                            safe=False,
                            status=400)
            else:
                return JsonResponse({'Error': 'Not valid pairs'},status=400)
        else:
            return JsonResponse({'Error': 'Number of players not power of two'},status=400)


@token_required
def tour_status(request, tournament_id):
    tour_obj = Tournament.objects.get(id=tournament_id, creator=request.swiss_user)
    status = tour_obj.status
    # if status != 3:
    no_of_players = TournamentPlayer.objects.filter(
        tournament=tour_obj).count()
    check_no_of_players = ispower(no_of_players, 2)
    rounds = int(math.log(no_of_players, 2))
    if check_no_of_players is True:
        return JsonResponse({'success': 'can start the match', 'rounds': rounds}, status=200)
        # else:
        #     return JsonResponse(
        #         {'failed': 'number of players must be a power of two'},
        #         status=400)


# @token_required
# def match_view(request, tournament_id, round_no):
#     tournament_obj = Tournament.objects.get(id=tournament_id)
#     match_obj = Match.objects.filter(tournament=tournament_obj, round=round_no)
#     match_dict = {}
#     i = 1
#     for match in match_obj:
#         player1 = match.player_one
#         player2 = match.player_two
#         winner = match.winner
#         if winner == player1:
#             loser = player2
#         else:
#             loser = player1
#         match_dict[i] = [{'match_id': match.id,
#                           'player1': player1.name, 'player2': player2.name,
#                           'winner': winner.name, 'loser': loser.name}]
#         i = i + 1
#     return JsonResponse(match_dict)


@token_required
def tour_standings_view(request, tournament_id, round_no):
    tournament_obj = Tournament.objects.get(id=tournament_id)
    no_of_players = TournamentPlayer.objects.filter(
        tournament=tournament_obj).count()
    total_rounds = int(math.log(no_of_players, 2))
    if int(round_no) <= total_rounds:
        swiss = tournament_obj.swiss_pairings(round_no)
    else:
        return JsonResponse({'error': 'Match has Completed'}, status=400)

    return JsonResponse({'data': swiss})


@token_required
def standings_view(request):
    tournament_id = request.GET.get('tournament', '')
    round_no = request.GET.get('round', '')
    tournament_obj = Tournament.objects.get(id=tournament_id,
                                            creator=request.swiss_user)
    player_standings = tournament_obj.player_standings(int(round_no))
    output =[]
    #import pdb; pdb.set_trace()
    for player, stats in player_standings:
        item = {}
        item['player_id'] = player.player.id
        item['player_name'] = player.player.name
        item['wins'] = stats['w']
        item['losses'] = stats['l']
        item['matches'] = stats['t']
        output.append(item)
    return JsonResponse(output, safe=False)
    # players = tournament_obj.players.all()
    # standings_list = []
    # if int(round_no) == 1:
    #     for player in players:
    #         player_standings = {}
    #         no_of_wins = Match.objects.filter(winner=player.player,round=round_no).count()
    #         matches_played = Match.objects.filter(
    #                 Q(player_one=player.player) |
    #                 Q(player_two=player.player)).count()
    #         loss = matches_played - no_of_wins
    #         player_standings['player_id'] = player.player.id
    #         player_standings['player_name'] = player.player.name
    #         player_standings['wins'] = no_of_wins
    #         player_standings['losses'] = loss
    #         standings_list.append(player_standings)
    #     return JsonResponse(standings_list,safe=False)
    # else:
    #     for player in players:
    #         player_standings = {}
    #         no_of_wins = Match.objects.filter(winner=player.player,round=round_no).count()
    #         matches_played = Match.objects.filter(
    #                 Q(player_one=player.player) |
    #                 Q(player_two=player.player)).count()
    #         loss = matches_played - no_of_wins
    #         player_standings['player_id'] = player.player.id
    #         player_standings['player_name'] = player.player.name
    #         player_standings['wins'] = no_of_wins
    #         player_standings['losses'] = loss
    #         standings_list.append(player_standings)
    #         return JsonResponse(standings_list,safe=False)


@token_required
def swiss_pairings_view(request):
    #import pdb;pdb.set_trace()
    tournament_id = request.GET.get('tournament_id', '')
    round_no = request.GET.get('round_num', '')
    tournament_obj = Tournament.objects.get(id=tournament_id,
                                            creator=request.swiss_user)
    #if (int(round_no) <= int(tournament_obj.current_round)) and tournament_obj.status < 3:
    # tournament_obj = Tournament.objects.get(id=tournament_id,
    #                                         creator=request.swiss_user)
    pairs = tournament_obj.swiss_pairings(round_no)
    print(pairs)
    return JsonResponse(pairs,safe=False)
    # else:
    #     return JsonResponse("Round Number is not valid,earlier match has not been played",safe=False)


@token_required
def getmatch_view(request, tournament_id, round_no):
    match_list = []
    match_details = {}
    tour_obj = Tournament.objects.get(id=tournament_id,creator=request.swiss_user)
    match_obj = Match.objects.filter(tournament = tour_obj, round=round_no)
    if match_obj:
        for match in match_obj:
            match_details = {}
            player1_obj = Player.objects.get(id=match.player_one_id)
            player2_obj = Player.objects.get(id=match.player_two_id)
            winner_obj = Player.objects.get(id = match.winner_id)
            rounds = match.round
            match_details['Player1'] = player1_obj.name
            match_details['Player2'] = player2_obj.name
            match_details['round'] = match.round
            match_details['Match Id'] = match.id
            match_details['Winner'] = winner_obj.name
            match_details['Winner_id'] = winner_obj.id
            if winner_obj.id == player1_obj.id:
                match_details['Loser'] = player2_obj.name
                match_details['Loser Id'] = player2_obj.id
            else:
                match_details['Loser'] = player1_obj.name
                match_details['Loser Id'] = player1_obj.id
            match_list.append(match_details)
        return JsonResponse({'Matches':match_list},status=200)
    else:
        return JsonResponse("Invalid round number",safe=False,status=400)


@token_required
def report_match_view(request, tournament_id, round_no):
    tournament_obj = Tournament.objects.get(id=tournament_id)
    if tournament_obj.current_round == int(round_no):
        req_body = json.loads(request.body.decode('utf-8'))
        player_one_id = req_body['player_one']
        player_two_id = req_body['player_two']
        winner_id = req_body['winner']
        loser_id = req_body['loser']
        obj = Tournament.objects.get(id=tournament_id, current_round=round_no)
        result = obj.play_match(player_one_id, player_two_id,
                                winner_id, loser_id, round_no)
        # if result == True:
        return JsonResponse({'succes': 'match played'})
    else:
        return JsonResponse({'failure':'Previous rounds has not been played'})


@token_required
def reset_db(request):
    MODELS = [Tournament, Player, TournamentPlayer, Match, User, UserProfile]
    for Model in MODELS:
        Model.objects.all().delete()
    return JsonResponse({"ok": "ok"})


@token_required
def getplayers_view(request):
    tournament_id = request.GET.get('tournament', '')
    tournament_obj = Tournament.objects.get(id=tournament_id)
    players = tournament_obj.players.all()
    tour_names_list = []
    for player in players:
        tournament_players = {}
        tournament_players['id'] = player.player.id
        tournament_players['name'] = player.player.name
        tour_names_list.append(tournament_players)
    return JsonResponse(tour_names_list,safe=False)


@token_required
def currentround_view(request, tournament_id):
    tournament_obj = Tournament.objects.get(id=tournament_id)
    current_round = tournament_obj.current_round
    return JsonResponse(current_round,safe=False)


@token_required
def roundresult_view(request, tournament_id, round_num):
    #import pdb;pdb.set_trace()
    match_details = []
    match_obj = Match.objects.filter(tournament = tournament_id, round=round_num)
    for rounds in match_obj:
        match = []
        player1_obj = Player.objects.get(id=rounds.player_one_id)
        match.append(player1_obj.name)
        player2_obj = Player.objects.get(id=rounds.player_two_id)
        match.append(player2_obj.name)
        winner_obj = Player.objects.get(id = rounds.winner_id)
        match.append(winner_obj.name)
        match_details.append(match)
        print(match_details)
    return JsonResponse(match_details,safe=False)

