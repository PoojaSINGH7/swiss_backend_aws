from django.http import JsonResponse
from django.contrib.auth import authenticate
import json
from core.decorators import token_required
import jwt
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.contrib.auth import logout
from .models import UserProfile
from django.conf import settings
SECRET_KEY = settings.SECRET_KEY


def sign_up_view(request):
    req_data = json.loads(request.body.decode('utf-8'))
    username = req_data['username']
    password = req_data['password']
    fullname = req_data['fullname']
    country = req_data['country']
    try:
        user = User.objects.get(username=username)
    except ObjectDoesNotExist:
        user = User.objects.create_user(username=username,
                                        password=password,
                                        first_name=fullname)
        user_country = UserProfile(user=user, country_code=country)
        user_country.save()
        token = jwt.encode({'user_id': user.id}, SECRET_KEY)
        return JsonResponse({'token': token.decode("utf-8"), 'success':'true'},status=200)
    else:
        return JsonResponse({'error': 'user already exists'},status=400)


def login_view(request):
    req_data = json.loads(request.body.decode('utf-8'))
    username = req_data['username']
    password = req_data['password']
    user = authenticate(username=username, password=password)
    if user is None:
        return JsonResponse(
            {"error": "Invalid credential"},
            status=400
        )
    else:
        token = jwt.encode({'user_id': user.id}, SECRET_KEY)
        return JsonResponse({'token': token.decode("utf-8"),'success':'true'}, status=200)


def logout_view(request):
    logout(request)
    return JsonResponse({'success':'true'},status=200)


@token_required
def hello_view(request):
    return JsonResponse({"hello": "world"})
